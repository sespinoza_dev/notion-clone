import React from 'react';
import { IBlock } from '../types/block/block.types'

const CustomTag = (level: string) => `h${level}`;

const Block: React.FC<{ block: IBlock }> = ({ block }) => {
  switch (block.type) {
    case 'text':
      return <p>{block.content}</p>;
    case 'image':
      return <img src={block.source} alt={block.caption} />;
    case 'video':
      return <video src={block.source} controls></video>;
    case 'audio':
      return <audio src={block.source} controls></audio>;
    case 'code':
      return <pre><code className={`language-${block.language}`}>{block.content}</code></pre>;
    case 'quote':
      return (
        <blockquote>
          <p>{block.content}</p>
          {block.citation && <cite>{block.citation}</cite>}
        </blockquote>
      );
    case 'embed':
      return (
        <div>
          <iframe src={block.source} frameBorder="0"></iframe>
          {block.caption && <p>{block.caption}</p>}
        </div>
      );
    // case 'database':
    //   return null;
    // case 'linked_database':
    //   return null;
    // case 'page':
    //   return null;
    // case 'table':
    //   return null;
    case 'heading':
      const HeadingTag = `h${block.level}` as keyof JSX.IntrinsicElements;
      return <HeadingTag>{block.text.map((textBlock, index) => <Block key={index} block={textBlock} />)}</HeadingTag>;

    default:
      return null;
  }
};

export default Block;
