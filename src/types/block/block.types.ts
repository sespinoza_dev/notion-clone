type TextBlock = {
  type: "text";
  content: string;
};

type HeadingBlock = {
  type: "heading";
  text: TextBlock[];
  level: 1 | 2 | 3 | 4 | 5 | 6;
};

type ImageBlock = {
  type: "image";
  source: string;
  caption?: string;
};

type VideoBlock = {
  type: "video";
  source: string;
  caption?: string;
};

type AudioBlock = {
  type: "audio";
  source: string;
  caption?: string;
};

type CodeBlock = {
  type: "code";
  content: string;
  language: string;
};

type QuoteBlock = {
  type: "quote";
  content: string;
  citation?: string;
};

type EmbedBlock = {
  type: "embed";
  source: string;
  caption?: string;
};

type BasicBlock = TextBlock | ImageBlock | VideoBlock | AudioBlock | CodeBlock | QuoteBlock | EmbedBlock;

// type DatabaseBlock = {
//     type: "database";
//     title: string;
//     schema: DatabaseSchema;
//     data: DatabaseData;
// };

type LinkedDatabaseBlock = {
  type: "linked_database";
  database_id: string;
};

type PageBlock = {
  type: "page";
  title: string;
  content: BasicBlock[];
};

// type TableBlock = {
//   type: "table";
//   title: string;
//   schema: TableSchema;
//   data: TableData;
// };

export type IBlock = BasicBlock | LinkedDatabaseBlock | PageBlock | HeadingBlock;
