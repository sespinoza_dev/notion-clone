import React, { useState } from 'react';
import './App.css';
import Block from './components/block'
import { IBlock } from './types/block/block.types'

const uid = () => {
  return Date.now().toString(36) + Math.random().toString(36).substr(2);
};

function App() {
  const blocks: IBlock[] = [
    {
      type: 'heading',
      text: [{
        type: 'text',
        content: 'My title h1!'
      }],
      level: 1
    },
    {
      type: 'text',
      content: 'This is a paragraph'
    },
    {
      type: 'code',
      content: 'echo $ENV_CODE',
      language: 'bash'
    },
    {
      type: 'image',
      source: 'https://picsum.photos/200/300',
      caption: 'random image'
    },
    {
      type: 'embed',
      source: 'http://www.example.com/',
      caption: 'some caption',
    },
    {
      type: 'quote',
      content: 'post tenebras lux',
      citation: 'Protestant Reformation',
    }
  ];

  return (
    <div className="App">
      <div>
        {blocks.map((block, index) => <Block key={index} block={block} />)}
      </div>
    </div>
  );
}

export default App;
